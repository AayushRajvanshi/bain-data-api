const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const cors = require('cors');
const morgan = require('morgan');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');
const session = require('express-session');

const configDB = require('./config/database.js');

mongoose.connect(configDB.url, {
  useMongoClient: true
});

mongoose.Promise = global.Promise;

app.set('view engine', 'ejs');
app.use(cors());
// app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: ['key']
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

//Fetching all routes
require('./routes/index.js')(router, passport);
require('./config/passport')(passport);

//Setting Base Route
app.use('/', router);

app.listen(port, (err) => {
  if (err) {
    console.log(err);
  };
});

module.exports = app;