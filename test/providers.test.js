process.env.NODE_ENV = 'test';
let mongoose = require("mongoose");
let Provider = require('../models/Provider');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
chai.use(chaiHttp);
describe('Providers', () => {
    describe('/GET providers', () => {
        it('it should GET all the providers', (done) => {
            chai.request(server)
                .get('/providers-without-auth')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });
});