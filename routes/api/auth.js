module.exports = (router, passport) => {
    router.get('/',
        (req, res) => {
            res.render('index.ejs', { message: req.flash('signupMessage') });
        }
    );
    router.get('/signup',
        (req, res) => {
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        }
    );
    router.get('/login',
        (req, res) => {
            res.render('login.ejs', { message: req.flash('loginMessage') });
        }
    );
    router.post('/signup',
        passport.authenticate('local-signup'),
        (req, res) => {
            res.redirect('/providers');
        }
    );
    router.post('/login',
        passport.authenticate('local-login'),
        (req, res) => {
            res.redirect('/providers');
        }
    );

    router.get('/current_user', (req, res) => {
        res.send(req.user);
    });

    router.get('/logout', (req, res) => {
        req.logout();
        res.redirect('/');
    });

}