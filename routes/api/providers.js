const Provider = require('../../models/Provider');
const requireLogin = require('../../middlewares/requireLogin');

module.exports = (router) => {
    router.get('/providers-without-auth', function (req, res) {
        let limit = parseInt(req.query.limit);
        let page = parseInt(req.query.page);
        let state = req.query.state;
        let max_discharges = req.query.max_discharges;
        let min_discharges = req.query.min_discharges;
        let max_average_covered_charges = req.query.max_average_covered_charges;
        let min_average_covered_charges = req.query.min_average_covered_charges;
        let max_average_medicare_payments = req.query.max_average_medicare_payments;
        let min_average_medicare_payments = req.query.min_average_medicare_payments;
        Provider.getProviders(function (err, providers) {
            if (err) {
                throw err;
            }
            res.json(providers);
        }, limit, page, state, max_discharges, min_discharges, max_average_covered_charges, min_average_covered_charges, max_average_medicare_payments, min_average_medicare_payments)
    });
    router.get('/providers', requireLogin, function (req, res) {
        let limit = parseInt(req.query.limit);
        let page = parseInt(req.query.page);
        let state = req.query.state;
        let max_discharges = req.query.max_discharges;
        let min_discharges = req.query.min_discharges;
        let max_average_covered_charges = req.query.max_average_covered_charges;
        let min_average_covered_charges = req.query.min_average_covered_charges;
        let max_average_medicare_payments = req.query.max_average_medicare_payments;
        let min_average_medicare_payments = req.query.min_average_medicare_payments;
        Provider.getProviders(function (err, providers) {
            if (err) {
                throw err;
            }
            res.json(providers);
        }, limit, page, state, max_discharges, min_discharges, max_average_covered_charges, min_average_covered_charges, max_average_medicare_payments, min_average_medicare_payments)
    });
}
