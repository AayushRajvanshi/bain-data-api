const fs = require('fs');
module.exports = (router, passport) => {
  fs.readdirSync(__dirname + '/api/').forEach((file) => {
    require(`./api/${file.substr(0, file.indexOf('.'))}`)(router, passport);
  });
};
