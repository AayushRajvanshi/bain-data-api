const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const Schema = mongoose.Schema;

const ProviderSchema = new Schema({
    "DRG Definition": String,
    "Provider Id": Number,
    "Provider Name": String,
    "Provider Street Address": String,
    "Provider City": String,
    "Provider State": String,
    "Provider Zip Code": Number,
    "Hospital Referral Region Description": String,
    "Total Discharges": Number,
    "Average Covered Charges": String,
    "Average Covered Charges Value": Number,
    "Average Total Payments": String,
    "Average Total Payments Value": Number,
    "Average Medicare Payments": String,
    "Average Medicare Payments Value": Number
});

ProviderSchema.plugin(mongoosePaginate);

const Provider = module.exports = mongoose.model('Provider', ProviderSchema);

//Get List of Providers
module.exports.getProviders = function (callback, limit, page, state, max_discharges, min_discharges, max_average_covered_charges, min_average_covered_charges, max_average_medicare_payments, min_average_medicare_payments) {
    let query = {};
    if (state) {
        query["Provider State"] = state.toUpperCase();
    }
    if (min_discharges) {
        query["Total Discharges"] = {
            '$gte': min_discharges
        }
    }
    if (max_discharges) {
        query["Total Discharges"] = {
            '$lte': max_discharges
        }
    }
    if (min_average_covered_charges) {
        query["Average Covered Charges Value"] = {
            '$gte': min_average_covered_charges
        }
    }
    if (max_average_covered_charges) {
        query["Average Covered Charges Value"] = {
            '$lte': max_average_covered_charges
        }
    }
    if (min_average_medicare_payments) {
        query["Average Medicare Payments Value"] = {
            '$gte': min_average_medicare_payments
        }
    }
    if (max_average_medicare_payments) {
        query["Average Medicare Payments Value"] = {
            '$lte': max_average_medicare_payments
        }
    }

    let options = {
        select: ['-_id','-DRG Definition', '-Provider Id', '-Average Covered Charges Value', '-Average Total Payments Value', '-Average Medicare Payments Value'],
        limit: limit || 10,
        page: page || 1
    }
    Provider.paginate(query, options, callback);
}